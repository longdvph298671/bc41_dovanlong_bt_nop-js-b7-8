


var arr = [];
function taoArr(){
    var newNumber = document.getElementById("new-number").value*1;
    arr.push(newNumber);
    document.getElementById('result-arr').innerHTML = `<h5>${arr}</h5>`
}

// cau 1
function tongSoDuong(){
    let sum = 0;
    for(var i = 0; i < arr.length; i++){
        if(arr[i] > 0){
            sum += arr[i];
        }
    }
    document.getElementById('result-one').innerHTML = `<h5>Tổng số dương: ${sum}</h5>`
}

// cau 2
function countSoDuong(){
    let count = 0;
    for(var i = 0; i < arr.length; i++){
        if(arr[i] > 0){
            count++;
        }
    }
    document.getElementById('result-two').innerHTML = `<h5>Số dương: ${count}</h5>`
}

// cau 3
function minArr(){
    console.log(arr)
    let min = arr[0];
    for(var i = 0; i < arr.length; i++){
        if(min > arr[i]){
            min = arr[i];
        }
    }
    document.getElementById('result-three').innerHTML = `<h5>Số nhỏ nhất: ${min}</h5>`
}

// cau 4
function soDuongNhoNhat(){
    let arrDuong = [];
    for(var i = 0; i < arr.length; i++){
        if(arr[i] > 0){
            arrDuong.push(arr[i]);
        }
    }
    if(arrDuong.length == 0){
        document.getElementById('result-four').innerHTML = `<h5>Không có số dương trong mảng</h5>`
    }
    else {
        let minArrDuong = arrDuong[0];
        for(var i = 0; i < arrDuong.length; i++){
            if(minArrDuong > arrDuong[i]){
                minArrDuong = arrDuong[i];
            }
        }
        document.getElementById('result-four').innerHTML = `<h5>Số dương nhỏ nhất: ${minArrDuong}</h5>`
    }
    console.log(arrDuong.length)
    console.log("🚀 ~ file: index.js:51 ~ soDuongNhoNhat ~ arrDuong", arrDuong)
}

// cau 5
function timSoChanCuoiCung(){
    var soChanCuoi = 0;
    for(var i = 0; i < arr.length; i++){
        if(arr[i]%2 == 0){
            soChanCuoi = arr[i];
        }
    }
    document.getElementById('result-five').innerHTML = `<h5>Số chẵn cuối cùng: ${soChanCuoi}</h5>`
}

// cau 6
function doiViTri(){
    let viTri1 = document.getElementById('vitri-1').value*1;
    let viTri2 = document.getElementById('vitri-2').value*1;

    var temp = arr[viTri1];
    arr[viTri1] = arr[viTri2];
    arr[viTri2] = temp;
    document.getElementById('result-six').innerHTML = `<h5>Mảng sau khi đổi: ${arr}</h5>`
}

// cau 7
function sapXep(){
    arr.sort();
    document.getElementById('result-seven').innerHTML = `<h5>Mảng sau khi sắp xếp: ${arr}</h5>`
}

// câu 8
function timSoNguyenTo(){
    for(var i = 0; i < arr.length; i++){
        if(arr[i] > 0){
            if(kiemTraSoNguyenTo(arr[i]) == true){
                document.getElementById('result-eight').innerHTML = `<h5> ${arr[i]}</h5>`
                break;
            }
            else{
                document.getElementById('result-eight').innerHTML = `<h5> ${-1}</h5>`
            }
        }
        else {
            document.getElementById('result-eight').innerHTML = `<h5> ${-1}</h5>`
        }
    }
}
function kiemTraSoNguyenTo(n){
    for(var i = 2; i <= Math.sqrt(n); i++){
        if(n % i == 0){
            return false;
        }
    }
    return true;
}

// cau 9
var arr2 = [];
function taoArr2(){
    let newNumber2 = document.getElementById('new-number2').value*1;
    arr2.push(newNumber2);
    document.getElementById('result-arr2').innerHTML = `<h5>${arr2}</h5>`
}
function countSoNguyen(){
    var chan = 0;
    for(var i = 0; i < arr2.length; i++){
        if(Number.isInteger(arr2[i]) == true){
            chan++;
        }
    }
    document.getElementById('result-nine').innerHTML = `<h5>Số nguyên: ${chan}</h5>`
}

// cau 10
function soSanh(){
    var soDuong = 0, soAm = 0;
    for(var i = 0; i < arr.length; i++){
        if(arr[i] > 0){
            soDuong++;
        }
        else if(arr[i] < 0){
            soAm++;
        }
    }
    if(soAm < soDuong){
    document.getElementById('result-ten').innerHTML = `<h5>Số dương > Số âm</h5>`
    }
    else if(soAm > soDuong){
        document.getElementById('result-ten').innerHTML = `<h5>Số âm > Số dương</h5>`
    }
    else {
        document.getElementById('result-ten').innerHTML = `<h5>Số âm = Số dương</h5>`
    }
}